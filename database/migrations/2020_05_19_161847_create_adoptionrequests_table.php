<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdoptionrequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adoptionrequests', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('cat_id');
            $table->string('name');
            $table->string('email');
            $table->string('gsm');
            $table->string('age');
            $table->longText('why');
            $table->string('time');
            $table->integer('count_animals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adoptionrequests');
    }
}
