@extends('layouts.main')

@section('title', 'Zwerfkat in leuven')
		
@section('content')
<section class="adoptieform">
<form id="formulier" method="post" action="{{ route('site.adoptcatrequest') }}">
@csrf
<div class="formulier_groep">
    <div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text">Naam</span>
    </div>
        <input id="name" type="text" class="form-control" placeholder="Vul in" name="name">
        @error('name')
        <br><span class="error" id="name_error" style="color:red;">{{$message}}</span>
        @enderror
    </div>

    <div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text">E-mail</span>
    </div>
        <input id="email" type="text" class="form-control" placeholder="Vul in" name="email">
        @error('email')
        <br><span class="error" id="email_error"  style="color:red;">{{$message}}</span>
        @enderror
    </div>

    <div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text">Gsm nummer</span>
    </div>
        <input id="gsm" type="text" class="form-control" placeholder="Vul in" name="gsm">
        @error('gsm')
        <br><span class="error" id="gsm_error"  style="color:red;">{{$message}}</span>
        @enderror
    </div>

    <div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text">Leeftijd</span>
    </div>
        <input id="age" type="text" class="form-control" placeholder="Vul in" name="age">
        @error('age')
        <br><span class="error" id="age_error"  style="color:red;">{{$message}}</span>
        @enderror
    </div>

    <div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text">Welke kat wil je adopteren?</span>
    </div>
    <select id="cat_id" class="form-control" name="cat_id" class="custom-select" id="inputGroupSelect01">
        @foreach($cats as $cat)
                <option name="cat_id" value="{{ $cat->id }}">{{ $cat->name }}</option>
        @endforeach
    </select>
    </div>

    <div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text">Waarom wil je deze kat adopteren?</span>
    </div>
        <input id="why" type="text" class="form-control" placeholder="Vul in" name="why">
        @error('why')
        <br><span class="error" id="why_error"  style="color:red;">{{$message}}</span>
        @enderror
    </div>

    <div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text">Hoeveel tijd kan je eraan spenderen per dag?</span>
    </div>
        <input id="time" type="text" class="form-control" placeholder="Vul in" name="time">
        @error('time')
        <br><span class="error" id="time_error"  style="color:red;">{{$message}}</span>
        @enderror
    </div>

    <div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text">Hoeveel huisdieren heb je al?</span>
    </div>
        <input id="count_animals" type="text" class="form-control" placeholder="Vul in" name="count_animals">
        @error('count_animals')
        <br><span class="error" id="count_animals_error"  style="color:red;">{{$message}}</span>
        @enderror
    </div>

    <input id="knop_verstuur" class="btn" type="submit" value="Verzend je aanvraag">
</div>
</form>
</section>
@endsection