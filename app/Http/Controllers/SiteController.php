<?php

namespace App\Http\Controllers;

use App\Cat;
use App\Adoptionrequest;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index()
    {
        //$cats = Cat::all();
        //return view('site.index', compact('cats'));
        return view('site.index');
    }

    public function adopt()
    {
        $cats = Cat::all();
        return view('site.adopt', compact('cats'));
    }

    public function contact()
    {
        return view('site.contact');
    }

    public function steun()
    {
        return view('site.steun');
    }

    public function werking()
    {
        return view('site.werking');
    }

    public function adoptcat()
    {
        $adoptionrequest = new Adoptionrequest;
        $cats = Cat::all();
        return view('site.adoptcat', compact('adoptionrequest', 'cats'));
    }

    public function adoptcatrequest(Request $request, Cat $cat){

        $request->validate([
            'cat_id' => 'required|numeric',
            'name' => 'required',
            'email' => 'required',
            'gsm' => 'required',
            'age' => 'required|numeric',
            'why' => 'required',
            'time' => 'required',
            'count_animals' => 'required|numeric'
        ]);

        $adoptionrequest = new Adoptionrequest;
        $adoptionrequest->cat_id = $request->cat_id;
        $adoptionrequest->name = $request->name;
        $adoptionrequest->email = $request->email;
        $adoptionrequest->gsm = $request->gsm;
        $adoptionrequest->age = $request->age;
        $adoptionrequest->why = $request->why;
        $adoptionrequest->time = $request->time;
        $adoptionrequest->count_animals = $request->count_animals;

        $adoptionrequest->save();

        return \redirect()->route('site.adopt');

    }




}
