@csrf
<div class="input-group mb-3">
<div class="input-group-prepend">
    <span class="input-group-text">Naam</span>
</div>
    <input type="text" class="form-control" placeholder="Name" name="name" value="{{ old('name', $cats->name) }}">
</div>
@error('name')
    <span class="error" style="color:red;">{{$message}}</span>
@enderror

<div class="input-group mb-3">
<div class="input-group-prepend">
    <span class="input-group-text">Leeftijd</span>
</div>
    <input type="text" class="form-control" placeholder="Leeftijd in jaren" name="age" value="{{ old('age', $cats->age) }}">
</div>
@error('age')
    <span class="error" style="color:red;">{{$message}}</span>
@enderror

<div class="input-group mb-3">
<div class="input-group-prepend">
    <span class="input-group-text">Kleur</span>
</div>
    <input type="text" class="form-control" placeholder="Kleur" name="color" value="{{ old('color', $cats->color) }}">
</div>
@error('color')
    <span class="error" style="color:red;">{{$message}}</span>
 @enderror

<div class="input-group mb-3">
<div class="input-group-prepend">
    <span class="input-group-text">Afkomst</span>
</div>
    <input type="text" class="form-control" placeholder="Afkomst" name="from" value="{{ old('from', $cats->from) }}">
</div>
@error('from')
    <span class="error" style="color:red;">{{$message}}</span>
@enderror

<div class="input-group mb-3">
  <div class="input-group-prepend">
  </div>
  <input type="checkbox" placeholder="Sociaal" name="social" value="{{ old('social', $cats->social) }}" {{ $cats->social ? "checked" : "" }}>
  <label style="margin-left: 10px;" for="social">Is de kat sociaal</label>
</div>

<div class="input-group mb-3">
<div class="input-group-prepend">
</div>
    <input type="checkbox" placeholder="Alleen blijven" name="alone" value="{{ old('alone', $cats->alone) }}" {{ $cats->alone ? "checked" : "" }}>
    <label style="margin-left: 10px;" for="alone">Kan de kat alleen blijven</label>
</div>

<div class="input-group mb-3">
<div class="input-group-prepend">
</div>
    <input type="checkbox" placeholder="Zindelijk" name="clean" value="{{ old('clean', $cats->clean) }}" {{ $cats->clean ? "checked" : "" }}>
    <label style="margin-left: 10px;" for="clean">Is de kat zindelijk</label>
</div>

<div class="input-group mb-3">
<div class="input-group-prepend">
    <span class="input-group-text">Afbeeldingsnaam</span>
</div>
    <input type="text" class="form-control" placeholder="Afbeeldingsnaam.jpg" name="image" value="{{ old('image', $cats->image) }}">
</div>

<div class="input-group mb-3">
<div class="input-group-prepend">
    <span class="input-group-text">Extra info</span>
</div>
    <input type="text" class="form-control" placeholder="Extra info" name="extra" value="{{ old('extra', $cats->extra) }}">
</div>

