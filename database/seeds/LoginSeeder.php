<?php

use App\User;
use Illuminate\Database\Seeder;

class LoginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        $user = new User;
        $user->name = "Lotte";
        $user->email = "lotte.van.lint@hotmail.com";
        $user->password = Hash::make('wachtwoord');
        $user->save();
    }
}
