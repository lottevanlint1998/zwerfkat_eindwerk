<!DOCTYPE html>
<html lang="en">

<head>
@section('meta')
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> 
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/favicon.png') }}"/>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
@show
    <title>@yield('title')</title>
</head>

<body>
<header id="home">

    <div class="navigation">
            <div class="topnav mobilenav">
                <div class="logo">
                <a href="{{ route('site.index') }}" class="active">
                    <img src="{{ asset('/images/logo_white.png') }}" alt="Logo">
                </a>
                <a href="{{ route('site.index') }}"><h1>Zwerfkat <br> <span>in Leuven</span></h1></a>
                </div>
                <div id="myLinks">
                    <a class="{{ (request()->is('/')) ? 'active' : '' }}" href="{{ route('site.index') }}">Home</a>
                    <a class="{{ (request()->is('site/adopt')) ? 'active' : '' }}" href="{{ route('site.adopt') }}">Adopteer</a>
                    <a class="{{ (request()->is('site/steun')) ? 'active' : '' }}" href="{{ route('site.steun') }}">Steun ons</a>
                    <a class="{{ (request()->is('site/werking')) ? 'active' : '' }}" href="{{ route('site.werking') }}">Onze werking</a>
                    <a class="{{ (request()->is('site/contact')) ? 'active' : '' }}" href="{{ route('site.contact') }}">Contact</a>
                </div>
                <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                    <i class="fa fa-bars"></i>
                </a>
            </div>
            <div class="desktopnav">
                <div class="logo">
                    <a href="{{ route('site.index') }}" class="active">
                        <img src="{{ asset('/images/logo_white.png') }}" alt="Logo">
                    </a>
                    <a href="{{ route('site.index') }}"><h1>Zwerfkat <br> <span>in Leuven</span></h1></a>
                </div>
                <nav>
                    <ul>
                        <li><a class="{{ (request()->is('/')) ? 'active' : '' }}" href="{{ route('site.index') }}">Home</a></li>
                        <li><a class="{{ (request()->is('site/adopt')) ? 'active' : '' }}" href="{{ route('site.adopt') }}">Adopteer</a></li>
                        <li><a class="{{ (request()->is('site/steun')) ? 'active' : '' }}" href="{{ route('site.steun') }}">Steun ons</a></li>
                        <li><a class="{{ (request()->is('site/werking')) ? 'active' : '' }}" href="{{ route('site.werking') }}">Onze werking</a></li>
                        <li><a class="{{ (request()->is('site/contact')) ? 'active' : '' }}" href="{{ route('site.contact') }}">Contact</a></li> 
                    </ul>
                </nav>
            </div>
    </div>
</header>

		<section>
            @yield('content')
            <a href="#home" id="go-up"><span class="fa fa-arrow-up"></span></a>
		</section>
        <footer>
            <div>
            <p>&copy; copyright zwerfkat in Leuven 2020</p>
            </div>
            <div>
                <div class="social">
                    <a target="_blank" href="https://www.facebook.com/zwerfkatinleuven"><i class="fa fa-facebook"></i></a>
                </div>
            </div>

        </footer>
        <script src="{{ asset('js/script.js') }}"></script>
    </body>

</html>