@extends('layouts.main')

@section('title', 'Zwerfkat in leuven')
		
@section('content')
<section class="container">
<div class="contact-opties">
	<div>
		<h3>Katje gevonden? </h3>
		<a target="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSc9OgLWtxm_i9LqLUMqEe6OSV7q1XZWrnQIpxSy3Lo3koC5dw/viewform"> Klik hier</a>
	</div>
	<div>
		<h3>Opvanggezin worden? </h3>
		<a target="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSeLTHEw_1YlZJZGcm3e9TksD_3-LyynzeKZTPuCYn1uMNLljA/viewform?fbclid=IwAR15wkKbcNba0iIkIG93nbEiatElncTyFwEOGWolHV2cYMd9MchW4NQHhzY"> Klik hier</a>
	</div>
	<div>
		<h3>Kat adopteren? </h3>
		<a href="{{ route( 'site.adoptcat') }}"> Klik hier</a>
	</div>
</div>

<form id="formulier">
	<div class="formulier_groep">
	<h3>Contactformulier</h3>
        <p>
            <label for="onderwerp">Kies een onderwerp:</label>
            <select id="onderwerp">
                <option value="meter of peter worden">Wordt meter of peter</option>
                <option value="sponsor worden">Wordt sponsor</option>
                <option value="doneer">Maak een donatie</option>
                <option value="vrijwilliger worden">Wordt vrijwilliger</option>
                <option value="andere">Andere</option>
            </select>
            <span class="error" id="onderwerp_error"></span>
        </p>
        <p>
          <label for="name">Naam:</label
          ><input type="text" id="name" />
          <span class="error" id="name_error"></span>
        </p>
        <p>
          <label for="email">Email:</label>
          <input type="text" id="email" />
          <span class="error" id="email_error"></span>
		</p>
		<p>
          <label for="telefoonnr">Telefoon nummer:</label>
          <input type="text" id="telefoonnr" />
          <span class="error" id="telefoonnr_error"></span>
        </p>
        <p>
          <label for="vraag">Vraag:</label>
          <textarea name="vraag" id="vraag" cols="20" rows="10"></textarea>
          <span class="error" id="vraag_error"></span>
        </p>
        <p>
            <input type="button" value="Verstuur" id="knop_verstuur" />
          
		</p>
		</div>
      </form>
	</section>
<script language="JavaScript" type="text/javascript">
"use strict";

(() => {
				/* Model */
				const model = {
					init: function () {
					},

					/* Constructor */
					PersoonGegevens: function (
						onderwerp,
						name,
						email,
						telefoonnr,
						vraag
					) {
						this.onderwerp = onderwerp;
						this.name = name;
						this.email = email;
						this.telefoonnr = telefoonnr;
						this.vraag = vraag;
					},
				};

				/* View */
				const view = {
					formulier: document.querySelector("#formulier"),
					onderwerp: document.querySelector("#onderwerp"),
					name: document.querySelector("#name"),
					email: document.querySelector("#email"),
					telefoonnr: document.querySelector("#telefoonnr"),
					vraag: document.querySelector("#vraag"),

					btnVerstuur: document.querySelector("#knop_verstuur"),

					dataPtagError: null,
					timeout: null,

					setError: function (id, tekst) {
						document.getElementById(id + "_error").innerHTML = tekst;

						if (tekst != "") {
							controller.data.allesCorrectIngevuld = false;
						}
					},

					showOkMessage: function () {
						const pTag = document.createElement("p");
						

						view.formulier.append(pTag);
					},

					clearInputs: function () {
						this.name.value = null;
						this.email.value = null;
						this.telefoonnr.value = null;
						this.vraag.value = null;
					},

					setInputs: function (gegevens) {
						this.onderwerp.value = gegevens.onderwerp;
						this.name.value = gegevens.name;
						this.email.value = gegevens.email;
						this.telefoonnr.value = gegevens.telefoonnr;
						this.vraag.value = gegevens.vraag;
					},
				};

				/* Controller */
				const controller = {
					data: {
						allesCorrectIngevuld: true,
					},

					init: function () {
						view.btnVerstuur.addEventListener("click", this.verstuur);
					},

					leegVeldControle: function (tekst, idee) {
						if (tekst.length == 0) {
							view.setError(idee, "Vul in a.u.b.");
							return false;
						} else {
							view.setError(idee, "");
							return true;
						}
					},

					getValue: function (idee) {
						return document.getElementById(idee).value;
					},

					checkEmail: function (email) {
						const regExp = /^[a-z](\.?[a-z0-9+_-]){0,}@[a-z0-9-]+\.([a-z]{1,20}\.)?[a-z]{2,20}$/;
						return regExp.test(email);
					},

					verstuur: function (e) {
						controller.data.allesCorrectIngevuld = true;

						const onderwerpTxt = controller.getValue("onderwerp");
						const naamTxt = controller.getValue("name");
						const emailTxt = controller.getValue("email");
						const telefoonnrTxt = controller.getValue("telefoonnr");
						const vraagTxt = controller.getValue("vraag");

						if (controller.leegVeldControle(onderwerpTxt, "onderwerp")) {
							if (onderwerpTxt.length < 2) {
								view.setError("onderwerp", "Minstens 2 karakters lang!");
							}
						}

						if (controller.leegVeldControle(naamTxt, "name")) {
							if (naamTxt.length < 2) {
								view.setError("name", "Minstens 2 karakters lang!");
							}
						}

						if (controller.leegVeldControle(emailTxt, "email")) {
							if (!controller.checkEmail(emailTxt)) {
								view.setError("email", "Dit lijkt niet correct!");
							}
						}
						
						if (controller.leegVeldControle(telefoonnrTxt, "telefoonnr")) {
							if (telefoonnrTxt.length != 10) {
								view.setError("telefoonnr", "Een telefoonnummer heeft 10 cijfers!");
							}
							if (isNaN(telefoonnrTxt)) {
								view.setError("telefoonnr", "Je moet getallen gebruiken!");
							}
						}
                        
                        if (controller.leegVeldControle(vraagTxt, "vraag")) {
							if (vraagTxt.length < 2) {
								view.setError("vraag", "Minstens 2 karakters lang!");
							}
						}

						if (controller.data.allesCorrectIngevuld) {
							const gegevens = new model.PersoonGegevens(
								onderwerpTxt,
								naamTxt,
								emailTxt,
								telefoonnrTxt,
								vraagTxt
							);

							view.clearInputs();
							controller.mail(gegevens);
						}
					},

					mail: function (gegevens) {
						const link =
							"mailto:lotte.van.lint@hotmail.com" +
							"?cc=lotte.van.lint@hotmail.com" +
							"&subject=" +
							encodeURIComponent(gegevens.onderwerp) +
							"&body=" +
							"naam: " +
							encodeURIComponent(gegevens.name) +
							encodeURIComponent("\r\n\n") +
							"email: " +
							encodeURIComponent(gegevens.email) +
							encodeURIComponent("\r\n\n") +
							"telefoonnummer: " +
							encodeURIComponent(gegevens.telefoonnr) +
							encodeURIComponent("\r\n\n") +
							"vraag: " +
							encodeURIComponent(gegevens.vraag);

						window.location.href = link;
					},
				};

				model.init();
				controller.init();
			})();

</script>
@endsection