@extends('layouts.main')

@section('title', 'Zwerfkat in leuven')
		
@section('content')

<header class="adopt_header">
    <h2>Adopt don't shop! <br> Bekijk hieronder zeker onze poezen ter adoptie.</h2>
    <a class="btn_adopt" href="{{ route('site.adoptcat') }}">Ik wil een kat adopteren</a>	
</header>
<article class="cats container">
    <div class="grid-container">
    @foreach($cats as $cat)
    <div class="cat grid-item">
        <div class="img_cat" style="background-image: url('{{ asset('/images/cats/' . $cat->image) }}');">
        </div>
        <div class="catinfo">
            <h3>{{ $cat->name }}</h3>
            <ul>
                <li><strong>Nummer:</strong> {{ $cat->id }}</li>
                <li><strong>Leeftijd:</strong> {{ $cat->age }} jaar</li>
                <li><strong>Kleur:</strong> {{ $cat->color }}</li>
                <li><strong>Afkomst:</strong> {{ $cat->from }}</li>
                <li><strong>Sociaal:</strong> {{ $cat->social ? "Ja" : "Nee" }}</li>
                <li><strong>Kan alleen blijven:</strong> {{ $cat->alone ? "Ja" : "Nee" }}</li>
                <li><strong>Zindelijk:</strong> {{ $cat->clean ? "Ja" : "Nee" }}</li>
                <li><strong>Extra informatie:</strong> {{ $cat->extra }}</li>
            </ul>
        </div>
    </div>
    @endforeach
    </div>


</article>


@endsection