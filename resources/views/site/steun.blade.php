@extends('layouts.main')

@section('title', 'Zwerfkat in leuven')
		
@section('content')

<header class="header_steun">
    <h2>Hoe kan je ons helpen?<br>
    <span>Er zijn 8 manieren!</span></h2>
</header>
<article class="steun_ons">
    <article class="desktop_steun">
        <article class="ipad_steun">
            <div>
                <span>Optie #1</span>
                <h2>Storten</h2>
                <h3>BE71 9731 3317 4869</h3>
                <p>Vermeld GIFT bij de betaling. <br> Ps: bedragen vanaf €40 zijn fiscaal aftrekbaar.</p>
            </div>
            <div>
                <span>Optie #2</span>
                <h2>Onze webshop</h2>
                <h3>Super leuke gadgets voor je kat.</h3>
                <p>Binnekort komt de webshop online!</p>
            </div>
        </article>
        <article class="ipad_steun">
            <div>
                <span>Optie #3</span>
                <h2>Word meter of peter</h2>
                <h3>De poezen zullen je dankbaar zijn.</h3>
                <a href="{{ route('site.contact') }}">
                    <i class="fa fa-envelope"></i>
                    <p>Mail ons</p>
                </a>
            </div>
            <div>
                <span>Optie #4</span>
                <h2>Zet ons in je testament</h2>
                <h3>Weet je niet wat doen met je erfenis?</h3>
            </div>
        </article>
    </article>
    <article class="desktop_steun">
        <article class="ipad_steun">
            <div>
                <span>Optie #5</span>
                <h2>Shop bij Bol.com</h2>
                <h3>Wij krijgen hier een procent op.</h3>
                <a target="_blank" href="https://www.bol.com/nl/?Referrer=ADVNLPPcefd5800cdbf929700a314f019060042797&utm_source=42797&utm_medium=Affiliates&utm_campaign=CPS&utm_content=ban">
                <i class="fa fa-arrow-right"></i>
                    <p>Ga naar Bol.com</p>
                </a>
            </div>
            <div>
                <span>Optie #6</span>
                <h2>Word sponsor</h2>
                <h3>Kan je wat spullen missen?</h3>
                <a href="{{ route('site.contact') }}">
                    <i class="fa fa-envelope"></i>
                    <p>Mail ons</p>
                </a>
            </div>
        </article>
        <article class="ipad_steun">
            <div>
                <span>Optie #7</span>
                <h2>Doneer</h2>
                <h3>We hebben 5 sponsorpakketten.</h3>
                <a href="{{ route('site.contact') }}">
                    <i class="fa fa-envelope"></i>
                    <p>Mail ons</p>
                </a>
            </div>
            <div>
                <span>Optie #8</span>
                <h2>Word vrijwilliger</h2>
                <h3>Kom ons een handje helpen.</h3>
                <a href="{{ route('site.contact') }}">
                    <i class="fa fa-envelope"></i>
                    <p>Mail ons</p>
                </a>
            </div>
        </article>
    </article>
</article>

@endsection