@extends('layouts.admin')

@section('title', 'zwerfkat - admin')
		
@section('content')
<a style="margin: 20px; margin-left: 0;" class="btn btn-primary" href="{{ route('admin.index') }}">Back</a>
		<h1>Maak nieuwe adoptiekat aan</h1>
		
            <form method="post" action="{{ route('admin.store') }}">
			@include('admin.includes.form')

			
			<input class="btn" type="submit" value="Voeg toe">

		</form>
@endsection