@extends('layouts.admin')

@section('title', 'zwerfkat - bewerk kat')
		
@section('content')
<a style="margin: 20px; margin-left: 0;" class="btn btn-primary" href="{{ route('admin.index') }}">Back</a>
		<h1>bewerk kat</h1>

		<form method="post" action="{{ route('admin.update', $cats->id) }}">
			@method('PUT')
			@include('admin.includes.form')
			<p>
				<input class="btn btn-primary" type="submit" value="bewerk">
			</p>
		</form>
@endsection