<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/favicon.png') }}"/>
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<header id="home">
<div class="topnav mobilenav">
                <div class="logo">
                <a href="{{ route('site.index') }}" class="active">
                    <img src="{{ asset('/images/logo_white.png') }}" alt="Logo">
                </a>
                <a href="{{ route('site.index') }}"><h1>Zwerfkat <br> <span>in Leuven</span></h1></a>
                </div>
</div>
<div class="desktopnav">
            <div class="logo">
            <a href="{{ route('site.index') }}" class="active">
                <img src="{{ asset('/images/logo_white.png') }}" alt="Logo">
            </a>
            <a href="{{ route('site.index') }}"><h1>Zwerfkat <br> <span>in Leuven</span></h1></a>
            </div>
</div>
        
</header>
    <div id="app">
        

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
