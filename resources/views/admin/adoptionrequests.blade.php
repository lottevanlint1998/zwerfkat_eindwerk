@extends('layouts.admin')

@section('title', 'Adoptiekatten')
		
@section('content')

<a style="margin: 20px; margin-left: 0;" class="btn btn-primary" href="{{ route('admin.index') }}">Back</a>
	<h1>Alle aanvragen</h1>

	<div class="row">
	@foreach($adoptionrequests as $adoptionrequest)
		<div class="col-lg-4 col-md-6 col-sm-12 cat">
			<h2>Kat: {{ \App\Cat::where(['id' => $adoptionrequest->cat_id])->first()->name }}</h2>
			<div style="background-image: url('{{ asset('/images/cats/' . \App\Cat::where(['id' => $adoptionrequest->cat_id])->first()->image) }}'); background-repeat: no-repeat; background-postiion: center; background-size: cover; width: 300px; height: 300px;"></div>
			<ul>
                <li><strong>Naam: </strong>{{ $adoptionrequest->name }}</li>
				<li><strong>Email: </strong>{{ $adoptionrequest->email }}</li>
                <li><strong>Gsm: </strong>{{ $adoptionrequest->gsm }}</li>
                <li><strong>Leeftijd: </strong>{{ $adoptionrequest->age }}</li>
                <li><strong>Waarom adopteren: </strong>{{ $adoptionrequest->why }}</li>
                <li><strong>Tijd die ze eraan kunnen spenderen: </strong>{{ $adoptionrequest->time }}</li>
				<li><strong>Aantal huisdieren ze hebben: </strong>{{ $adoptionrequest->count_animals }}</li>
			</ul>
            <a class="btn btn_actions" href="mailto:{{ $adoptionrequest->email }}">Mail hun</a>
		</div>
		@endforeach
	</div>

@endsection