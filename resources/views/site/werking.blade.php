@extends('layouts.main')

@section('title', 'Zwerfkat in leuven')
		
@section('content')
<section class="werking container">
<article>
    <h2>Ieder katje telt!</h2>
    <p>Zwerfkat in Leuven is een Leuvense VZW die zich voornamelijk richt op het vangen en verzorgen en steriliseren/castreren van zwerfkatten en de opvang van kittens en 
        tamme volwassen katten. Kittens en gevonden tamme volwassen katten (verdwaald, gedumpt, afgestaan) komen bij opvanggezinnen terecht.  We zijn dus geen klassiek 
        asiel met kooitjes.  We zoeken altijd actief (via de chip) naar de baasjes van de opvangkatjes.  Indien zij niet gevonden worden dan komen de beestjes ter adoptie.  
        Volwassen verwilderde katten worden teruggeplaatst op hun vangplek en opgevolgd door voederouders.
    De vereniging werkt voor 100% met vrijwilligers en is voor het merendeel afhankelijk van giften en donaties. Alles gebeurt dus na de gewone werkuren. Ons werkgebied 
    is beperkt tot Leuven en de deelgemeenten.</p>
</article>
<article>
    <h2>Hoe verloopt een vangactie?</h2>
    <p>Wij krijgen soms tientallen oproepen per dag binnen. Dit kan gebeuren via  e-mail, onze speciale voice-mailbox of berichten via Facebook. Ook de Leuvense politie geeft meldingen door.
    Iemand van onze vereniging contacteert de melder voor meer details (aantal katten, eventuele gewonde of zieke dieren) en bepaalt op basis van deze informatie of het over een dringende 
    actie of over een minder-dringende actie gaat. Hiervoor beschikken we momenteel over 15 vangkooien.</p>
    <h2>Heel belangrijk!</h2>
    <p>Iedereen wil zo snel mogelijk geholpen worden. Wij zijn echter een vrijwilligersorganisatie en kunnen niet overal tegelijkertijd zijn. Daarom is het belangrijk om ons zoveel mogelijk
         informatie te geven, zodat we kunnen bepalen wat dringend is of niet</p>
</article>
<article>
    <h2>No kill beleid</h2>
    <h3>Wat wil dat zeggen?</h3>
    <p>Zwerfkat in Leuven heeft een No Kill Beleid. We gaan enkel over tot euthanasie als geen andere optie meer mogelijk is. 
         De beslissing wordt na overleg met de dierenartsen genomen. Enkele voorbeelden:</p>
    <ul>
        <li>FIP</li>
        <li>Aanrijding met te zware verwondingen</li>
        <li>Ernstige genetische afwijkingen waardoor een katwaardig leven niet mogelijk is</li>
        <li>Verwilderde aids-katten – risico verdere verspreiding ziekte</li>
    </ul>
</article>
<article>
    <h2>Wij castreren of steriliseren en chippen iedere kat bij ons</h2>
   <p>Sinds 1 november 2017 moeten alle kittens gechipt worden en geregistreerd worden in de databank CatID voor ze 12 weken oud zijn. Ook alle katten die van eigenaar 
       veranderen, moeten gechipt en geregistreerd worden. Deze wet geldt ook voor katten en kittens die gratis weggegeven worden.</p>
</article>
<article>
    <h2>Castratie & sterilisatie</h2>
    <p>Sinds 1 september 2014 moeten alle katten en kittens gecastreerd of gesteriliseerd worden vóór de verkoop of de adoptie, 
        ook als ze gratis worden weggeven! Bijkomend moeten sinds 1 april 2018 ook alle vanaf dan geboren kittens gecastreerd of gesteriliseerd 
        worden voor ze 5 maanden oud zijn. Katten die geboren zijn tussen 31 augustus 2014 en 31 maart 2018 moeten steriel gemaakt zijn tegen 1 januari 2020. 
        Een uitzondering is voorzien voor katten die naar een erkende fokker (met HK-nummer) gaan of die voor het buitenland bestemd zijn,
         deze moeten niet steriel gemaakt worden.</p>
</article>
</section>
@endsection