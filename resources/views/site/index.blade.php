@extends('layouts.main')

@section('title', 'Zwerfkat in leuven')
        
@section('content')
<header id="header">
    <h1>Welkom!</h1>
</header>

<div class="container">
<article class="corona">
    <span>!</span>
    <h2>Corona update!</h2>
    <p>We mogen en blijven meldingen over katten in nood behandelen. We gaan dit natuurlijk doen met de noodzakelijke voorzorgsmaatregelen tegen het virus. Dit betekent onder andere dat we geen rechtstreeks fysiek contact zullen maken met de melder(s). We verwachten ook meer werk te krijgen, omdat het kittenseizoen reeds van start is gegaan. We vragen je wel om geduld te hebben.  Aarzel niet om ons te contacteren en vooral, zorg voor jezelf en elkaar!
    Het ZIL-team</p>
    <span>!</span>
</article>
</div>

<article class="actions">
    <div id="adopt">
        <a href="{{ route('site.adopt') }}">
            <h2>Adopteer een kat</h2>
            <img src="{{ asset('/images/icons/paw.png') }}" alt="">
        </a>
    </div>
    <div id="steun">
        <a href="{{ route('site.steun') }}">
            <h2>Steun ons</h2>
            <img src="{{ asset('/images/icons/help.png') }}" alt="">
        </a>
    </div>
</article>
<div class="container">
<article class="partners">
    <h2>Partners</h2>
    <div class="desktop_partners">
        <div class="ipad_partners">
            <div class="mobile_partners"><img src="{{ asset('/images/partners/partner1.jpg') }}" alt=""></div>
            <div class="mobile_partners"><img src="{{ asset('/images/partners/partner2.jpg') }}" alt=""></div>
        </div>
        <div class="ipad_partners">
            <div class="mobile_partners"><img src="{{ asset('/images/partners/partner3.jpg') }}" alt=""></div>
            <div class="mobile_partners"><img src="{{ asset('/images/partners/partner4.png') }}" alt=""></div>
        </div>
    </div>
    <div class="desktop_partners">
        <div class="ipad_partners">
            <div class="mobile_partners"><img src="{{ asset('/images/partners/partner5.jpg') }}" alt=""></div>
            <div class="mobile_partners"><img src="{{ asset('/images/partners/partner6.png') }}" alt=""></div>
        </div>
        <div class="ipad_partners">
            <div class="mobile_partners"><img src="{{ asset('/images/partners/partner7.jpg') }}" alt=""></div>
            <div class="mobile_partners"><img src="{{ asset('/images/partners/partner8.png') }}" alt=""></div>
        </div>
    </div>
</article>
</div>

@endsection