<?php

namespace App\Http\Controllers;

use App\Cat;
use App\Adoptionrequest;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        $cats = Cat::all();
        return view('admin.index', compact('cats'));
    }

    public function adoptionrequests()
    {
        $adoptionrequests = Adoptionrequest::all();
        $cat = Cat::where('id', 'cat_id')->first();
        return view('admin.adoptionrequests', compact('adoptionrequests', 'cat'));
    }

    public function create()
    {
        $cats = new Cat;
        return view('admin.create', compact('cats'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'color' => 'required',
            'from' => 'required',
        ]);

        $cats = new Cat;
        $cats->name = $request->name;
        $cats->age = $request->age;
        $cats->color = $request->color;
        $cats->from = $request->from;
        $cats->social = $request->has('social');
        $cats->alone = $request->has('alone');
        $cats->clean = $request->has('clean');
        $cats->image = $request->image;
        $cats->extra = $request->extra;

        $cats->save();

        return \redirect()->route('admin.index');
    }
    public function edit(Cat $cats)
    {
        return view('admin.edit', compact('cats'));
    }
    public function update(Request $request, Cat $cats)
    {
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'color' => 'required',
            'from' => 'required',
        ]);
        $cats->name = $request->name;
        $cats->age = $request->age;
        $cats->color = $request->color;
        $cats->from = $request->from;
        $cats->social = $request->has('social');
        $cats->alone = $request->has('alone');
        $cats->clean = $request->has('clean');
        $cats->image = $request->image;
        $cats->extra = $request->extra;

        $cats->save();

        return \redirect()->route('admin.index');
    }
    public function delete(Cat $cats)
    {
        $cats->delete();
        return \redirect()->route('admin.index');
    }
}
