<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//index
Route::get('/', 'SiteController@index')->name('site.index');

//site 
Route::get('/site/adopt', 'SiteController@adopt')->name('site.adopt');
Route::get('/site/contact', 'SiteController@contact')->name('site.contact');
Route::get('/site/steun', 'SiteController@steun')->name('site.steun');
Route::get('/site/werking', 'SiteController@werking')->name('site.werking');
Route::get('/site/adoptcat', 'SiteController@adoptcat')->name('site.adoptcat');
Route::post('/site/adoptcat', 'SiteController@adoptcatrequest')->name('site.adoptcatrequest');

//inloggen
Auth::routes();

Route::middleware(['auth'])->group(function(){
    Route::get('/admin', 'HomeController@index')->name('admin.index');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
//admin
Route::get('/admin', 'AdminController@index')->name('admin.index');
Route::get('admin/adoptionrequests', 'AdminController@adoptionrequests')->name('admin.adoptionrequests');
Route::get('/admin/create', 'AdminController@create')->name('admin.create');
Route::post('/admin/create', 'AdminController@store')->name('admin.store');
Route::get('/admin/edit/{cats}', 'AdminController@edit')->name('admin.edit');
Route::put('/admin/edit/{cats}', 'AdminController@update')->name('admin.update');
Route::get('admin/delete/{cats}', 'AdminController@delete')->name('admin.delete');

});