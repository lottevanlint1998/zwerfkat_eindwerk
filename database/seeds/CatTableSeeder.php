<?php

use App\Cat;
use Illuminate\Database\Seeder;

class CatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cats')->truncate();
        $cat = new Cat;
        $cat->name = "Cindy";
        $cat->age = 3;
        $cat->color = "Ros";
        $cat->from = "Gevonden in Leuven";
        $cat->social = 0;
        $cat->alone = 1;
        $cat->clean = 1;
        $cat->image = "cindy.jpg";
        $cat->extra = "Ze is nog erg verlegen";
        $cat->save();

        $cat = new Cat;
        $cat->name = "daisy";
        $cat->age = 0.3;
        $cat->color = "wit grijs";
        $cat->from = "Ontvangen vanuit Heusden";
        $cat->social = 1;
        $cat->alone = 0;
        $cat->clean = 0;
        $cat->image = "daisy.jpg";
        $cat->extra = "test";
        $cat->save();

        $cat = new Cat;
        $cat->name = "tiger";
        $cat->age = 5;
        $cat->color = "tijger";
        $cat->from = "Ontvangen vanuit Bertem";
        $cat->social = 1;
        $cat->alone = 1;
        $cat->clean = 1;
        $cat->image = "tiger.jpg";
        $cat->extra = "test";
        $cat->save();

        $cat = new Cat;
        $cat->name = "vic";
        $cat->age = 2;
        $cat->color = "wit grijs";
        $cat->from = "Gevonden onder een brug in Wijgmaal";
        $cat->social = 1;
        $cat->alone = 1;
        $cat->clean = 0;
        $cat->image = "vic.jpg";
        $cat->extra = "test";
        $cat->save();

        $cat = new Cat;
        $cat->name = "wiskas";
        $cat->age = 4;
        $cat->color = "lichtkleurige tijger";
        $cat->from = "Ontvangen vanuit Gent";
        $cat->social = 1;
        $cat->alone = 0;
        $cat->clean = 0;
        $cat->image = "wiskas.jpg";
        $cat->extra = "test";
        $cat->save();

        $cat = new Cat;
        $cat->name = "bolleke";
        $cat->age = 0.3;
        $cat->color = "wit met lichte tijgerstrepen";
        $cat->from = "Ontvangen vanuit Gent";
        $cat->social = 1;
        $cat->alone = 0;
        $cat->clean = 0;
        $cat->image = "bolleke.jpg";
        $cat->extra = "test";
        $cat->save();
    }
}
