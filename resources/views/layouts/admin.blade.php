<!DOCTYPE html>
<html lang="en">

<head>
@section('meta')
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/favicon.png') }}"/>
@show
    <title>Admin - zwerfkat - @yield('title')</title>
</head>

<body>
    <header>
            <div>
            <h1>Zwerfkat in Leuven</h1>
            </div>
            <div>
            <a class="btn" href="{{ route('logout') }}">Logout</a>
            </div>
    </header>
        <div class="container">

		<section>
			@yield('content')
		</section>
	</div>
        </div>
    </body>

</html>