@extends('layouts.admin')

@section('title', 'Adoptiekatten')
		
@section('content')

<div>
        <a style="margin: 20px; margin-left: 0;" class="btn" href="{{ route('admin.create') }}">Voeg nieuwe adoptiekat toe</a>
		</div>
		<div>
        <a style="margin: 20px; margin-left: 0;" class="btn" href="{{ route('admin.adoptionrequests') }}">Bekijk al de aanvragen</a>
        </div>

	<h1>Alle katten</h1>

	<div class="row">
	@foreach($cats as $cat)
		<div class="col-lg-4 col-md-6 col-sm-12 cat">
			<h2>Naam: {{ $cat->name }}</h2>
			<div style="background-image: url('{{ asset('/images/cats/' . $cat->image) }}'); background-repeat: no-repeat; background-postiion: center; background-size: cover; width: 300px; height: 300px;"></div>
			<ul>
				<li><strong>Leeftijd: </strong>{{ $cat->age }}</li>
                <li><strong>Kleur: </strong>{{ $cat->color }}</li>
                <li><strong>Afkomst: </strong>{{ $cat->from }}</li>
                <li><strong>Sociaal: </strong>{{ $cat->social ? "Ja" : "Nee" }}</li>
                <li><strong>Kan alleen blijven: </strong>{{ $cat->alone ? "Ja" : "Nee" }}</li>
				<li><strong>Is zindelijk: </strong>{{ $cat->clean ? "Ja" : "Nee" }}</li>
				<li><strong>Extra info: </strong>{{ $cat->extra }}</li>
			</ul>
			<a class="btn btn_actions" href="{{ route('admin.edit', $cat->id) }}">Edit</a>	
            <a class="btn" href="{{ route('admin.delete', $cat->id) }}">Delete</a>

		</div>
		@endforeach
	</div>

@endsection